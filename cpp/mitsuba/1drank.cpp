/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/render/sampler.h>

// STL includes
#include <fstream>
#include <random>

// Include rank-1 header
#include <rank1/rank1.hpp>


MTS_NAMESPACE_BEGIN

class RankLatticeSampler : public Sampler {
public:
    RankLatticeSampler() : Sampler(Properties()) { }

    RankLatticeSampler(const Properties &props) : Sampler(props) {
        /* Number of samples per pixel when used with a sampling-based integrator */
        m_sampleCount = props.getSize("sampleCount", 4);
        m_maxDimension = 8u;

        std::random_device rd;
        m_gen = std::mt19937(rd());

        for(uint8_t d=0; d<64; ++d) {
            m_offset_i[d] = m_gen() % 128u;
            m_offset_j[d] = m_gen() % 128u;
        }

        // Create the rank-1 lattice sampler
        // TODO load from file
        //

        // Allocate and populate shift tile
        //
        m_tileResolution = 128u;
        m_shifts = new Float[m_tileResolution*m_tileResolution*m_maxDimension];
        std::uniform_real_distribution<Float> dist(0.0, 1.0);
        for(uint32_t i=0; i<m_tileResolution*m_tileResolution*m_maxDimension; ++i) {
           m_shifts[i] = dist(m_gen);
        }
        m_scrambles = new uint32_t[m_tileResolution*m_tileResolution*m_maxDimension];
        for(uint32_t i=0; i<m_tileResolution*m_tileResolution*m_maxDimension; ++i) {
           m_scrambles[i] = m_gen();
        }
    }

    RankLatticeSampler(Stream *stream, InstanceManager *manager)
     : Sampler(stream, manager) {
        // m_random = static_cast<Random *>(manager->getInstance(stream));
    }

    void serialize(Stream *stream, InstanceManager *manager) const {
        Sampler::serialize(stream, manager);
        // manager->serialize(stream, m_random.get());
    }

    ref<Sampler> clone() {
        ref<RankLatticeSampler> sampler = new RankLatticeSampler();
        sampler->m_sampleCount = m_sampleCount;
        memcpy(sampler->m_offset_i, m_offset_i, sizeof(m_offset_i));
        memcpy(sampler->m_offset_j, m_offset_j, sizeof(m_offset_j));
        sampler->m_maxDimension = m_maxDimension;
        sampler->m_tileResolution = m_tileResolution;

        // Copy CPR shifts
        sampler->m_shifts = new Float[m_tileResolution*m_tileResolution*m_maxDimension];
        uint32_t size = m_tileResolution*m_tileResolution*m_maxDimension;
        std::copy(m_shifts, m_shifts+size, sampler->m_shifts);
        
        // Copy scrambles
        sampler->m_scrambles = new uint32_t[m_tileResolution*m_tileResolution*m_maxDimension];
        std::copy(m_scrambles, m_scrambles+size, sampler->m_scrambles);

        // Copy samples
        sampler->m_rank1 = m_rank1;

        return sampler.get();
    }

    void generate(const Point2i &pos) {
        m_pixelPosition = pos;
        m_sampleIndex   = 0;
        m_dimension     = 0;
        m_dimension1DArray = m_dimension2DArray = 0;
    }
    
    void advance() {
        m_dimension   = 0;
        m_sampleIndex = m_sampleIndex + 1;
    }

    Float shift(const Point2i& p, uint32_t dim) const {
       uint32_t d = dim % m_maxDimension;
       uint32_t i = p.x % m_tileResolution;
       uint32_t j = p.y % m_tileResolution;
       uint32_t k = i + j*m_tileResolution;
       return m_shifts[k*m_maxDimension + d];
    }
    
    uint32_t scramble(const Point2i& p, uint32_t dim) const {
       uint32_t d = dim % m_maxDimension;
       uint32_t i = p.x % m_tileResolution;
       uint32_t j = p.y % m_tileResolution;
       uint32_t k = i + j*m_tileResolution;
       return m_scrambles[k*m_maxDimension + d] % m_sampleCount;
    }

    // Access the samples by padding a 2D sequence with
    // random index scrambling and different shifts.
    //
    Float sample(uint32_t k, uint32_t d, Float shift, uint32_t scramble) const {
       d = d % 2u; // m_rank1.m_dimension;
       //k = k ^ scramble;
       return m_rank1.rank1_float(k, d, shift);
    }

    Float next1D() {
        Float s = shift(m_pixelPosition, m_dimension);
        uint32_t S = scramble(m_pixelPosition, m_dimension);
        Float x = sample(m_sampleIndex, m_dimension, s, S);

        m_dimension += 2u;
        return Point1(x);
    }

    Point2 next2D() {
        if(m_dimension == 0u) {
            m_dimension += 2u;
            return Point2(0.5, 0.5);
        }

        Float s, x, y;
        uint32_t S;
        s = shift(m_pixelPosition, m_dimension+0u);
        S = scramble(m_pixelPosition, m_dimension+0u);
        x = sample(m_sampleIndex, m_dimension+0u, s, S);
        s = shift(m_pixelPosition, m_dimension+1u);
        S = scramble(m_pixelPosition, m_dimension+1u);
        y = sample(m_sampleIndex, m_dimension+1u, s, S);

        m_dimension += 2u;
        return Point2(x,y);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "1DRankLatticeSampler[" << endl
            << "  sampleCount = " << m_sampleCount << endl
            << "]";
        return oss.str();
    }

    MTS_DECLARE_CLASS()
private:
    uint32_t m_dimension;
    Point2i  m_pixelPosition;
    uint32_t m_offset_i[64];
    uint32_t m_offset_j[64];
    Float*   m_shifts;
    uint32_t* m_scrambles;
    uint32_t m_tileResolution;
    uint32_t m_maxDimension;

    rank1    m_rank1; // rank-1 lattice sampler

    std::mt19937 m_gen;
};

MTS_IMPLEMENT_CLASS_S(RankLatticeSampler, false, Sampler)
MTS_EXPORT_PLUGIN(RankLatticeSampler, "1D Rank Lattice sampler");
MTS_NAMESPACE_END

