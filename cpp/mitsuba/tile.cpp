/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/render/sampler.h>

// Eigen includes
#include <Eigen/Core>
#include<Eigen/StdVector>

// STL includes
#include <fstream>
#include <random>

/* Tile stores the key to the sequence
 */
struct Tile {
   Tile() {}
   Tile(const std::string& filename) {
       load_tile_mts(filename);
   }


   uint32_t R;
   uint32_t NbSPP;
   uint32_t NbDim = 2u;

   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                        Eval part                           //
   //                                                            //
   ////////////////////////////////////////////////////////////////

    float sample(uint32_t i, uint32_t j, uint32_t k, uint32_t d) {
        i = i % this->R;
        j = j % this->R;
        return this->samples[i + j*this->R](k,d);
    }


   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                      IO routines                           //
   //                                                            //
   ////////////////////////////////////////////////////////////////

   void load_tile_mts(const std::string filename) {
      std::ifstream file(filename.c_str());
      unsigned int t;
      file >> t; this->R = t;
      file >> t; this->NbSPP = t;
      file >> t; this->NbDim = t;
      std::cout << "<<Loading tile>> " << this->R << ", " << this->NbSPP  << ", " << this->NbDim << std::endl;
      this->samples = std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf>>(R*R);
      
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            this->samples[i + j*this->R] = Eigen::MatrixXf(this->NbSPP, this->NbDim);
            for(uint32_t k=0; k<this->NbSPP; ++k) {
               for(uint32_t d=0; d<this->NbDim; ++d) {
                  float sample;
                  file >> sample;
                  this->samples[i + j*this->R](k,d) = sample;
               }
            }
         }
      }
   }

   std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf>> samples;
};


MTS_NAMESPACE_BEGIN

class TileSampler : public Sampler {
public:
    TileSampler() : Sampler(Properties()) { }

    TileSampler(const Properties &props) : Sampler(props) {
        /* Number of samples per pixel when used with a sampling-based integrator */
        m_sampleCount = props.getSize("sampleCount", 4);
        std::string filename = props.getString("filename", "tile.samples");
        m_tile   = Tile(filename);


        std::random_device rd;
        m_gen = std::mt19937(rd());


        for(uint8_t d=0; d<64; ++d) {
            m_offset_i[d] = m_gen() % m_tile.R;
            m_offset_j[d] = m_gen() % m_tile.R;
        }
    }

    TileSampler(Stream *stream, InstanceManager *manager)
     : Sampler(stream, manager) {
        // m_random = static_cast<Random *>(manager->getInstance(stream));
    }

    void serialize(Stream *stream, InstanceManager *manager) const {
        Sampler::serialize(stream, manager);
        // manager->serialize(stream, m_random.get());
    }

    ref<Sampler> clone() {
        ref<TileSampler> sampler = new TileSampler();
        sampler->m_sampleCount = m_sampleCount;
        sampler->m_tile = m_tile;
        memcpy(sampler->m_offset_i, m_offset_i, sizeof(m_offset_i));
        memcpy(sampler->m_offset_j, m_offset_j, sizeof(m_offset_j));
        return sampler.get();
    }

    void generate(const Point2i &pos) {
        m_pixelPosition = pos;
        m_sampleIndex   = 0;
        m_dimension     = 0;
        m_dimension1DArray = m_dimension2DArray = 0;
    }
    
    void advance() {
        m_dimension   = 0;
        m_sampleIndex = m_sampleIndex + 1;
    }

    Float next1D() {
        if(m_sampleIndex < m_tile.NbSPP && m_dimension < m_tile.NbDim*64) {
            uint32_t pi = m_pixelPosition.x + m_offset_i[(m_dimension-2) /  m_tile.NbDim];
            uint32_t pj = m_pixelPosition.y + m_offset_j[(m_dimension-2) /  m_tile.NbDim];
            float value =  m_tile.sample(pi, pj, m_sampleIndex, (m_dimension) % m_tile.NbDim);
            m_dimension += 2u;
            return Point1(value);

        } else {
            m_dimension += 2u;
            return Point1(rand() / double(RAND_MAX));
        }
    }

    Point2 next2D() {
        if(m_dimension == 0u) {
            m_dimension += 2u;
            return Point2(0.5, 0.5);
        }

        if(m_sampleIndex < m_tile.NbSPP && m_dimension < m_tile.NbDim*64) {
            uint32_t pi = m_pixelPosition.x + m_offset_i[(m_dimension-2) /  m_tile.NbDim];
            uint32_t pj = m_pixelPosition.y + m_offset_j[(m_dimension-2) /  m_tile.NbDim];
            Float value1 = m_tile.sample(pi, pj, m_sampleIndex, (m_dimension++) % m_tile.NbDim);
            Float value2 = m_tile.sample(pi, pj, m_sampleIndex, (m_dimension++) % m_tile.NbDim);
            return Point2(value1, value2);
        } else {
            m_dimension += 2u;
            return Point2(rand() / double(RAND_MAX), rand() / double(RAND_MAX));
        }
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "TileSampler[" << endl
            << "  sampleCount = " << m_sampleCount << endl
            << "]";
        return oss.str();
    }

    MTS_DECLARE_CLASS()
private:
    uint32_t m_dimension;
    Point2i  m_pixelPosition;
    Tile     m_tile;
    uint32_t m_offset_i[64];
    uint32_t m_offset_j[64];

    std::mt19937 m_gen;
};

MTS_IMPLEMENT_CLASS_S(TileSampler, false, Sampler)
MTS_EXPORT_PLUGIN(TileSampler, "Tile sampler");
MTS_NAMESPACE_END

