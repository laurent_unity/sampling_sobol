// STL includes
#include <iostream>
#include <random>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <utility>
#include <bitset>

// Global includes
#include <sobol/sobol.hpp>

int main(int argc, char** argv)
{

    /* Loading Sobol direction numbers from file */
    sobol s("../data/new-joe-kuo-6.21201");

    // pixel key
    uint32_t key_u = 0;
    uint32_t key_v = 0;
    uint32_t key_w = 0;
    srand( time( NULL) );
   //  for(uint32_t bit=0 ; bit < 32 ; ++bit)
   //  {
   //     if(rand()%2) key_u += 1<<bit;
   //     if(rand()%2) key_v += 1<<bit;
   //     if(rand()%2) key_w += 1<<bit;
   //  }

    key_u = std::random_device()();
    key_v = std::random_device()();
   //  std::cout << "# " << key_u << std::endl;

    std::cout << key_u << std::endl;
    for(uint32_t i=0; i<128u; ++i) {
      /*
      std::cout << s.sobol_owen(i, 0u, 32u, key_u) << " ";
      std::cout << s.sobol_owen(i, 1u, 32u, key_v) << std::endl;
      */
      std::cout << s.sobol_float(i, 0u) << " ";
      std::cout << s.sobol_float(i, 1u) << std::endl;
    }
    

    return EXIT_SUCCESS;
}


