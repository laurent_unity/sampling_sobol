// STL includes
#include <cstdlib>
#include <chrono>
#include <iostream>

// Local includes
#include "sobol.h"

// My sobol includes
#include <sobol/sobol.hpp>


#define NB_SAMPLES    256
#define NB_DIMENSIONS 128

int main()
{
    sobol _sobol("joe-kuo-old.1111");
    
    uint32_t nb_fails = 0;
    for(uint32_t s=0; s<NB_SAMPLES; ++s)
    {
        for(uint32_t d=0; d<NB_DIMENSIONS; ++d)
        {
            uint8_t sample_g = sobol_gruenschloss::sample(s, d);
            uint8_t sample_o = _sobol.sobol_float(s, d);
            if(sample_g != sample_o) {
                nb_fails++;
            }
        }
    }
    std::cout << "Nb of fails: " << nb_fails << std::endl;

    if(nb_fails == 0) {
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}
