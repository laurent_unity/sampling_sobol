// STL includes
#include <cstdlib>
#include <chrono>
#include <iostream>

// Local includes
#include "sobol.h"

// My sobol includes
#include <sobol/sobol.hpp>


#define NB_SAMPLES    65536
#define NB_DIMENSIONS 128

int main()
{
    // Evaluate Gruenschloss's Sobol
    auto start = std::chrono::system_clock::now();
    float sample = 0.0f;
    for(uint32_t s=0; s<NB_SAMPLES; ++s)
    {
        for(uint32_t d=0; d<NB_DIMENSIONS; ++d)
        {
            sample = sobol_gruenschloss::sample(s, d);
        }
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff_gruenschloss = end-start;

    // Evaluate Gruenschloss's Sobol
    sobol _sobol("../data/new-joe-kuo-6.21201");
    start = std::chrono::system_clock::now();
    for(uint32_t s=0; s<NB_SAMPLES; ++s)
    {
        for(uint32_t d=0; d<NB_DIMENSIONS; ++d)
        {
            sample = _sobol.sobol_float(s, d);
        }
    }
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff_ours = end-start;

    std::cout << "Time to sample "  << NB_SAMPLES << " " << NB_DIMENSIONS << "D samples  : " << std::endl
              << " + Gruenschloss: " << diff_gruenschloss.count() << "s" << std::endl
              << " + Ours: " << diff_ours.count() << "s" << std::endl;

    if(diff_ours.count() < diff_gruenschloss.count()) {
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}
