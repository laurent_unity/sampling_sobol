// STL includes
#include <iostream>
#include <random>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <utility>
#include <bitset>

// Global includes
#include <sobol/sobol.hpp>

int main(int argc, char** argv)
{
    /* Constants */
    const uint32_t d = (argc > 1) ? std::atoi(argv[1]) : 0;

    /* Loading Sobol direction numbers from file */
    const std::string filename = (argc > 2) ? argv[2] : "../data/new-joe-kuo-6.21201";
    sobol s(filename.c_str());

    for(uint8_t i=0; i<32; ++i)
    {
        std::bitset<32> bits = s.sobol_mk[d][i];
        std::cout << uint32_t(i) << "\t" << bits << std::endl;
    }
    std::cout << std::endl;
    
    // Print the first entries of this dimension
    for(uint8_t i=0; i<16; ++i) {
        std::cout << uint32_t(i) << ": " << uint32_t( s.sobol_binary(i, d) ) << std::endl;
    }
    std::cout << std::endl;
    
    // XOR the indices
    //
    uint8_t xoring = 3u;
    for(uint8_t i=0; i<16; ++i) {
        uint8_t j = i ^ xoring;
        std::cout << uint32_t(i) << "->" << uint32_t(j) << ": " << uint32_t( s.sobol_binary(j, d) ) << std::endl;
    }
    std::cout << std::endl;
    

    // Hand made permutation of the rows
    //
    std::swap(s.sobol_mk[d][0], s.sobol_mk[d][1]);
    for(uint8_t i=0; i<16; ++i) {
        std::cout << uint32_t(i) << ": " << uint32_t( s.sobol_binary(i, d) ) << std::endl;
    }
    std::swap(s.sobol_mk[d][0], s.sobol_mk[d][1]);
    return EXIT_SUCCESS;
}

