#pragma once

// Defines vector, uint32_t
#include <vector>
#include <cstdint>
#include <cmath>

// Defines point
#include "optimizer.hpp"

bool is_dyadic(const std::vector<point>& pts) {
    const uint32_t size  = pts.size();

    /* allocate a counter list that stores the number of points falling
     * into the same cell.
     */
    std::vector<uint8_t> counter(size);

    /* Loop over all possible 2D sub-interval of size [wi,wj] */
    for(uint32_t wi=1u; wi<=size; wi*=2u)
    {
        const uint32_t wj = size/wi;

        // Reset the counter list
        uint8_t counter[wj][wi];
        memset(counter, 0u, size);

        // For each point in the points list, look at where it falls
        // in the counter cell and increment the cell's value.
        for(auto pt : pts) {
            const uint32_t i  = (pt.x*size) / wi;
            const uint32_t j  = (pt.y*size) / wj;
            const uint32_t id = i + j;
            assert(id < size);

            counter[i][j] += 1u;

            // Early exit if the cell has already two elements
            if(counter[i][j] > 1u) {
                return false;
            }
        }
    }

    return true;
}