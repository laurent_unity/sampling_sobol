#pragma once

// STL includes
#include <random>
#include <iostream>
#include <fstream>
#include <cstring>
#include <climits>
#include <cassert>
#include <stdint.h>

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

//#define FLIP_MORTON_BITS
//#define FLIP_MORTON_SHIFT

struct morton
{
    /* Scramling tree */
    uint8_t* m_scrambling_tree = nullptr;
    uint32_t m_scrambling_size = 0;

    /* Constructor
     * Can load a precomputed scrambling tree
     */
    morton() {} // default
    
    morton(const char* filename)
    {
        if(filename != nullptr)
        {
            std::ifstream file(filename, std::ifstream::binary);
            char temp[7]; temp[6] = 0;
            file.read(temp, 6u*sizeof(char));
            if( strcmp(temp, "MORTON") == 0 )
            {
                file.read((char*)&m_scrambling_size, sizeof(uint32_t));
                std::cerr << __FILENAME__ << ":" << __LINE__ << " "
                          << "using scrambling tree in \"" << filename << "\" "
                          << "of size=" << m_scrambling_size << std::endl;

                m_scrambling_tree = new uint8_t[m_scrambling_size];
                file.read((char*)m_scrambling_tree, m_scrambling_size*sizeof(uint8_t));
            }
            else
            {
                std::cerr << __FILENAME__ << ":" << __LINE__ << " "
                          << "no scrambling tree in \"" << filename << "\", "
                          << "keyword=\"" << temp << "\"" << std::endl;
            }
        }
    }

    /* Destructor */
    ~morton()
    {
        if(m_scrambling_tree != nullptr)
        {
            delete[] m_scrambling_tree;
        }
    }

    /* Save the scrambling tree
     */
    void save_scrambling_tree(const char* filename) const
    {
        if(m_scrambling_tree != nullptr)
        {
            std::ofstream file(filename, std::ofstream::binary);
            file << "MORTON";
            file.write((char*)&m_scrambling_size, sizeof(uint32_t));
            file.write((char*)m_scrambling_tree, m_scrambling_size*sizeof(uint8_t));

        } else {
            std::cerr << "[" << __FILENAME__ << ":" << __LINE__ << "] "
                      << "no scrambling tree allocated to be saved" << std::endl;
        }
    }


    /* Get the Morton (z-curve) code associated to pixel (x,y) for a grid
     * resolution of r x r.
     */
    inline
    uint32_t morton_index(uint32_t x, uint32_t y) const
    {
        uint32_t m = 0u; // Result: the morton code
        uint8_t  l = 0u; // Temp: the bit selector

        // While some bits are set in the binary representation of the
        // x and y indexes, add them to the morton representation.
        //
        // At the end, the binary form of x and y are interleaved into
        // the result `m`:
        //   m = x_0 y_0 x_1 y_1 ...
        //
        while(x != 0u || y != 0u)
        {
            assert(l < CHAR_BIT*sizeof(uint32_t));
            m |= (x & 1u) << (l++);
            m |= (y & 1u) << (l++);

            x >>= 1u;
            y >>= 1u;
        }
        return m;
    }

    /* Scramble a Morton curve index `i` using a random permutation tree.
     * The tree is seeded using `seed`.
     */
    inline
    uint32_t morton_scramble(uint32_t i, uint8_t m=8, uint32_t seed=0) const
    {

        /* Set of permutations of [[0..3]] -> [[0..3]] */
        static const
        uint8_t permutations[24][4] = {{0,1,2,3}, {0,1,3,2}, {0,2,1,3},
                                       {0,2,3,1}, {0,3,1,2}, {0,3,2,1},
                                       {1,0,2,3}, {1,0,3,2}, {1,2,0,3},
                                       {1,2,3,0}, {1,3,0,2}, {1,3,2,0},
                                       {2,0,1,3}, {2,0,3,1}, {2,1,0,3},
                                       {2,1,3,0}, {2,3,0,1}, {2,3,1,0},
                                       {3,0,1,2}, {3,0,2,1}, {3,1,0,2},
                                       {3,1,2,0}, {3,2,0,1}, {3,2,1,0}};

        // Create a random number generator with a specific seed. This rng is
        // a tree of random number that need to be accessed in a coherent maner
        // the random number are explorer in a breadth first order.
        static thread_local std::mt19937_64 gen;
        std::uniform_int_distribution<uint32_t> dist(0,23);
        
        // Variables
        uint32_t result = 0u;
        uint32_t shift  = 0u;
        uint32_t base   = 0u;

        // For each pair of bits, apply a random perturbation of the integer
        // it represent using the table of permuations `morton::permutations`.
        const uint8_t m2 = m/2u;
        for(uint8_t k=0; k<m2; ++k)
        {
            // Get the random index p
            gen.seed((seed + base + shift));
            const uint32_t p = (m_scrambling_tree != nullptr) ? m_scrambling_tree[base + shift] : dist(gen);

            const uint8_t ob = (i >> 2u*(m2-1u - k)) & 3u; // original bits
            const uint8_t pb = permutations[p][ob]; // permuted bits

            // Set the pair of bits
#ifdef FLIP_MORTON_BITS
            // We flip it with respect to the order of the initial index
            result |= (pb & 1u) << (2u*k+1u);
            result |= (pb & 2u) << (2u*k-1u);
#else
            result |= pb << 2u*(m2-1u - k);
#endif

            // Update the random shift
#ifdef FLIP_MORTON_SHIFT
            shift = (shift << 1u) | ((ob >> 1u) & 1u);
            shift = (shift << 1u) | (ob & 1u);
#else
            shift = (shift << 2u) | ob;
#endif
            base += 1u << (2u * k);

        }
        return result;
    }
};
