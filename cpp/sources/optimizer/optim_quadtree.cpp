// STL includes
#include <iostream>
#include <vector>
#include <cstdlib>
#include <array>
#include <cmath>
#include <random>

// Sobol includes
#include <sobol/sobol.hpp>

// cimg includes
#define cimg_display 0
#include <cimg/cimg.h>

struct Map {
    Map(uint32_t R) {
        this->R = R;
        this->values = std::vector<float>(R*R);
    }

    uint32_t R;
    std::vector<float> values;
};

struct Tree {
    Tree(uint32_t R) {
        for(uint32_t r=2; r<=R; r*=2) {
            levels.push_back( Map(r) );
        }
    }

    std::vector<Map> levels;
};

struct Tile {
    Tile(uint32_t R) {
        this->R = R;
        this->values = std::vector<uint32_t>(R*R);
        for(uint32_t i=0; i<R*R; ++i) {
            this->values[i] = i;
        }
    }

    uint32_t R;
    std::vector<uint32_t> values;
};

/* Test function */
float eval(float x, float y)
{
    // Gaussian center
    float x0 = 0.5f;
    float y0 = 0.5f;
    // Gaussian std
    float std = 0.5f;
    // eval
    float res = expf(- 0.5f * ((x-x0)*(x-x0) + (y-y0)*(y-y0)) / (std*std));
    return res;
}


/* Loading Sobol direction numbers from file */
const std::string filename = "../data/new-joe-kuo-6.21201";
const sobol s(filename.c_str());

/* This function loop over all the pixels and build the
   tree that contains the integral of the test function
   with the underlying sequence.
 */
void compute_tree(const Tile& tile, Tree* tree) {
    // Build the first level
    Map& level = tree->levels.back();
    for(uint32_t j=0; j<level.R; ++j) {
        for(uint32_t i=0; i<level.R; ++i) {
            uint32_t id = i + j*level.R;
            uint32_t k  = tile.values[id];
            level.values[id] = eval( s.sobol_float(k, 0u), s.sobol_float(k, 1u) );
        }
    }

    for(int32_t l=tree->levels.size()-2; l>=0; --l) {
        Map& level  = tree->levels[l];
        Map& leaves = tree->levels[l+1];
        
        for(uint32_t j=0; j<level.R; ++j) {
            for(uint32_t i=0; i<level.R; ++i) {
                uint32_t id  = i + j*level.R;
                level.values[id] = 0.0f;

                for(uint32_t u=0; u<2; ++u) {
                    for(uint32_t v=0; v<2; ++v) {
                        uint32_t id2 = (2*i+u) + (2*j+v)*leaves.R;
                        level.values[id] += leaves.values[id2];
                    }
                }
            }
        }
    }
}

std::vector<float> eval_cost(const uint32_t _p[2], Tree* tree) {

    uint32_t p[2] = {_p[0], _p[1]};

    std::vector<float> cost_vector;
    for(int32_t l=tree->levels.size()-1; l>=0; --l) {
        Map& level  = tree->levels[l];
        
        // Update pixel coordinates
        p[0] = p[0] / 2;
        p[1] = p[1] / 2;

        float cost = 0.0f;
        cost += fabs(level.values[(2*p[0]+0) + level.R*(2*p[1]+0)] - level.values[(2*p[0]+1) + level.R*(2*p[1]+0)]);
        cost += fabs(level.values[(2*p[0]+0) + level.R*(2*p[1]+0)] - level.values[(2*p[0]+0) + level.R*(2*p[1]+1)]);
        cost += fabs(level.values[(2*p[0]+0) + level.R*(2*p[1]+0)] - level.values[(2*p[0]+1) + level.R*(2*p[1]+1)]);
        cost += fabs(level.values[(2*p[0]+1) + level.R*(2*p[1]+0)] - level.values[(2*p[0]+0) + level.R*(2*p[1]+1)]);
        cost += fabs(level.values[(2*p[0]+1) + level.R*(2*p[1]+0)] - level.values[(2*p[0]+1) + level.R*(2*p[1]+1)]);
        cost += fabs(level.values[(2*p[0]+0) + level.R*(2*p[1]+1)] - level.values[(2*p[0]+1) + level.R*(2*p[1]+1)]);

        cost_vector.push_back(cost);
    }
    return cost_vector;
}

void swap_values(const uint32_t _pA[2], const uint32_t _pB[2], Tree* tree) {
    Map& level = tree->levels.back();

    uint32_t pA[2] = {_pA[0], _pA[1]};
    uint32_t pB[2] = {_pB[0], _pB[1]};

    float fA = level.values[pA[0] + level.R*pA[1]];
    float fB = level.values[pB[0] + level.R*pB[1]];

    // Propagate the permutation in the upper levels
    for(int32_t l=tree->levels.size()-1; l>=0; --l) {
        Map& level  = tree->levels[l];
        
        // Look at the change of values
        float vA = level.values[pA[0] + level.R*pA[1]];
        float vB = level.values[pB[0] + level.R*pB[1]];

        level.values[pA[0] + level.R*pA[1]] = vA - fA + fB;
        level.values[pB[0] + level.R*pB[1]] = vB - fB + fA;

        // Update pixel coordinates
        pA[0] = pA[0] / 2;
        pA[1] = pA[1] / 2;
        pB[0] = pB[0] / 2;
        pB[1] = pB[1] / 2;
    }
}

void swap_values_tile(const uint32_t pA[2], const uint32_t pB[2], Tile* tile) {
    uint32_t temp = tile->values[pA[0] + tile->R*pA[1]];
    tile->values[pA[0] + tile->R*pA[1]] = tile->values[pB[0] + tile->R*pB[1]];
    tile->values[pB[0] + tile->R*pB[1]] = temp;
}


bool permut_pixels(const uint32_t pA[2], const uint32_t pB[2], Tile* tile, Tree* tree, bool force=false) {

    auto costsA_old = eval_cost(pA, tree);
    auto costsB_old = eval_cost(pB, tree);

    swap_values(pA, pB, tree);
    swap_values_tile(pA, pB, tile);
    if(force) {
        return true;
    }

    auto costsA_new = eval_cost(pA, tree);
    auto costsB_new = eval_cost(pB, tree);

    const uint32_t nb_levels = costsA_new.size();

    /* // This one is not working.
    uint32_t nb_good = 0u;
    uint32_t max_level = std::min(nb_levels-1, uint32_t(T *nb_levels));
    float costNew = 0.0f, costOld = 0.0f;
    bool accept_mut = true;
    for(uint32_t k=nb_levels-1; k>=max_level; --k) {
        // uint32_t l = nb_levels-1 - k;
        uint32_t l = k;
        costNew = (costsA_new[l] + costsB_new[l]);
        costOld = (costsA_old[l] + costsB_old[l]);

        accept_mut &= costNew < costOld;
    }
    */

    auto costNew = (costsA_new[1] + costsB_new[1]);
    auto costOld = (costsA_old[1] + costsB_old[1]);
    bool accept_mut = costNew < costOld;

    costNew = (costsA_new[2] + costsB_new[2]);
    costOld = (costsA_old[2] + costsB_old[2]);
    accept_mut &= costNew < costOld;

    costNew = (costsA_new[3] + costsB_new[3]);
    costOld = (costsA_old[3] + costsB_old[3]);
    accept_mut &= costNew < costOld;

    if(accept_mut == false) {
        swap_values(pA, pB, tree);
        swap_values_tile(pA, pB, tile);
    }

    return accept_mut;
}

void randomize(uint32_t nb_mut, Tile* tile, Tree* tree) {
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<uint32_t> dist(0, tile->R-1);

    for(uint32_t k=0; k<nb_mut; ++k) {
        uint32_t pA[2] = { dist(gen), dist(gen) };
        uint32_t pB[2] = { dist(gen), dist(gen) };

        permut_pixels(pA, pB, tile, tree, true);
    }
}

void optimize(uint32_t nb_mut, Tile* tile, Tree* tree) {
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<uint32_t> dist(0, tile->R-1);

    uint32_t accepted_mut = 0u;
    for(uint32_t k=0; k<nb_mut; ++k) {
        uint32_t pA[2] = { dist(gen), dist(gen) };
        uint32_t pB[2] = { dist(gen), dist(gen) };

        bool has_permuted = permut_pixels(pA, pB, tile, tree, false);
        accepted_mut += (uint32_t)has_permuted;


    }
}

void save_tree(const std::string filename, const Tree& tree) {
    for(uint32_t l=0; l<tree.levels.size(); ++l) {
        auto level = tree.levels[l];
        auto img = cimg_library::CImg<float>(level.R, level.R, 1, 1);

        for(uint32_t j=0; j<level.R; ++j) {
            for(uint32_t i=0; i<level.R; ++i) {
                auto id = i + j*level.R;
                img(i, j, 0) = 255.0f*level.values[id];
            }
        }

        img.save(filename.c_str(), l);
    }
}

void save_tile(const std::string filename, const Tile& tile) {
    auto img = cimg_library::CImg<float>(tile.R, tile.R, 1, 3);

    for(uint32_t j=0; j<tile.R; ++j) {
        for(uint32_t i=0; i<tile.R; ++i) {
            auto index = tile.values[i + j*tile.R];
            img(i, j, 0) = 255.0f*s.sobol_float(index, 0u);
            img(i, j, 1) = 255.0f*s.sobol_float(index, 1u);
        }
    }

    img.save(filename.c_str());
}

int main(int argc, char** argv) {

    /* Global parameters of the algorithm: see command line args */
    uint32_t R = 128u; // Resolution of the tile

    /* Temporary data */
    Tree tree(R); // Quadtree with error in each cell
    Tile tile(R); // Tile containing the first index of the sequence

    compute_tree(tile, &tree);
    randomize(100000, &tile, &tree);

    save_tile("tile-before.png", tile);
    save_tree("tree-before.png", tree);

    for(uint32_t k=0; k<1000; ++k) {
        optimize(10000000, &tile, &tree);
        

        save_tile("tile.png", tile);
        save_tree("tree.png", tree);
    }

    return EXIT_SUCCESS;
}