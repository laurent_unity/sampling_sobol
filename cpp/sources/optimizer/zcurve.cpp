// STL includes
#include <iostream>
#include <vector>
#include <cstdlib>
#include <array>
#include <cmath>
#include <random>

// Sobol includes
#include <sobol/sobol.hpp>

// local includes
#include "morton.hpp"

// cimg includes
#define cimg_display 0
#include <cimg/cimg.h>

struct Tile {
    Tile(uint32_t R) {
        this->R = R;
        this->values = std::vector<float>(R*R);
    }

    uint32_t R;
    std::vector<float> values;
};

/* Test function */
float eval(float x, float y)
{
    // Gaussian center
    float x0 = 0.5f;
    float y0 = 0.5f;
    // Gaussian std
    float std = 0.5f;
    // eval
    float res = expf(- 0.5f * ((x-x0)*(x-x0) + (y-y0)*(y-y0)) / (std*std));
    //float res = expf(- 0.5f * ((y-y0)*(y-y0)) / (std*std));
    return res;
}


/* Loading Sobol direction numbers from file */
const std::string filename = "../data/new-joe-kuo-6.21201";
const sobol s(filename.c_str());

const morton curve;

/* This function loop over all the pixels and build the
   tree that contains the integral of the test function
   with the underlying sequence.
 */
void compute_tile(Tile* tile, uint32_t nb_spp, uint32_t shift, bool scramble=true) {
    
   std::random_device rd;  //Will be used to obtain a seed for the random number engine
   std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

   const uint32_t seed   = gen();
   const uint32_t levels = 2 * (log(tile->R) / log(2));

   std::cout << seed << ", " << levels << std::endl;

   // Build the first level
   for(uint32_t j=0; j<tile->R; ++j) {
      for(uint32_t i=0; i<tile->R; ++i) {
         uint32_t k  = curve.morton_index(i, j);
         if(scramble) {
            k = curve.morton_scramble(k, levels, seed);
         }
         k = k << shift;

         float value = 0.0f;
         for(uint32_t sk=0; sk<nb_spp; ++sk) {
            value += eval( s.sobol_owen(k+sk, 0u, 32u, seed), s.sobol_owen(k+sk, 1u, 32u, seed) );
         }
         tile->values[i + j*tile->R] = value / (float)nb_spp;
      }
   }
}

void save_tile(const std::string filename, const Tile& tile) {
    auto img = cimg_library::CImg<float>(tile.R, tile.R, 1, 1);

    for(uint32_t j=0; j<tile.R; ++j) {
        for(uint32_t i=0; i<tile.R; ++i) {
            auto v = tile.values[i + j*tile.R];
            img(i, j, 0) = 255.0*v;
        }
    }

    img.save(filename.c_str());
}

int main(int argc, char** argv) {

   if(argc != 3){
      return EXIT_FAILURE;
   }
   uint32_t nb_spp = std::atoi(argv[1]);
   uint32_t shift  = std::atoi(argv[2]);

   /* Global parameters of the algorithm: see command line args */
   uint32_t R = 128u; // Resolution of the tile

   /* Temporary data */
   Tile tile(R); // Tile containing the first index of the sequence

   compute_tile(&tile, nb_spp, shift, false);
   save_tile("tile.png", tile);

   compute_tile(&tile, nb_spp, shift, true);
   save_tile("tile-scrambled.png", tile);
   return EXIT_SUCCESS;
}
