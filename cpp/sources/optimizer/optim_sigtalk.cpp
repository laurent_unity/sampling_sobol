// STL includes
#include <iostream>
#include <vector>
#include <cstdlib>
#include <array>
#include <cmath>
#include <random>
#include <utility>
#include <fstream>

// Sobol includes
#include <sobol/sobol.hpp>
#include <rank1/rank1.hpp>

// cimg includes
#define cimg_display 0
#include <cimg/cimg.h>

// Eigen includes
#include <Eigen/Core>
#include<Eigen/StdVector>


// #define SAMPLES_RANDOM
// #define SAMPLES_OWEN
#define SAMPLES_OWEN_XOR
// #define SAMPLES_OWEN_ANTITHETIC
// #define SAMPLES_SOBOL_LAINE_KARRAS
//#define SAMPLES_1D_RANK_LATTICE


#if defined(SAMPLES_OWEN) || defined(SAMPLES_OWEN_XOR) || defined(SAMPLES_OWEN_ANTITHETIC) || defined(SAMPLES_SOBOL_LAINE_KARRAS)
/* Loading Sobol direction numbers from file */
const std::string filename = "../data/new-joe-kuo-6.21201";
const sobol _sobol(filename.c_str());
#elif defined(SAMPLES_1D_RANK_LATTICE)
const rank1 _rank1;
#endif

/* Tile stores the key to the sequence
 */
struct Tile {
   Tile(uint32_t NbSPP=1u, uint32_t R=128) {
      this->R = R;
      this->NbSPP = NbSPP;
      this->seeds = std::vector<uint32_t>(R*R);
      this->fvalues = std::vector<Eigen::VectorXf>(R*R);
      this->samples = std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf>>(R*R);

      this->x0 = 0.25f + 0.5*Eigen::MatrixXf::Random(this->NbIntegrands, 2).array();
      this->dir = Eigen::MatrixXf::Random(this->NbIntegrands, 2);
      this->dir.rowwise().normalize();

      std::cout << "# Generating seeds" << std::endl;
      generate_seeds();
      std::cout << "# Evaluating tile" << std::endl;
      eval_tile();
   }


   uint32_t R;
   uint32_t NbSPP;
   uint32_t NbIntegrands = 1024u;
   uint32_t NbDim = 2u;


   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                      Test Function                         //
   //                                                            //
   ////////////////////////////////////////////////////////////////

   Eigen::MatrixXf x0;
   Eigen::MatrixXf dir;

   float eval_cos(const Eigen::MatrixXf& x, uint32_t i)
   {
      float f   = float((i + 2) / 2);
      float res = ( M_PI_2 * f * (x).col(i % 2) ).array().cos().mean();
      return res;
   }

   float eval_power(const Eigen::MatrixXf& x, uint32_t i)
   {
      float f   = float((i + 2) / 2);
      auto  dot = ( x * dir.row(i) );  
      float res = Eigen::pow(dot.array(), f).mean();
      return res;
   }

   float eval_heaviside(const Eigen::MatrixXf& x, uint32_t i)
   {
      float res = 0.0f;
      
      for(uint32_t k=0; k<x.rows(); ++k) {
         Eigen::VectorXf _x = x.row(k);
         Eigen::VectorXf dx = _x;
         dx[0] -= x0.row(i)[0];
         dx[1] -= x0.row(i)[1];
         float dot = dx[0]*dir.row(i)[0] +  dx[1]*dir.row(i)[1];
         res += (dot > 0.0) / float(x.rows());
      }
      return res;
   }

   float eval_xy(const Eigen::MatrixXf& x)
   {
      float res = 100.0* ( x.col(0) + x.col(1) ).array().mean();
      return res;
   }


   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                        Init part                           //
   //                                                            //
   ////////////////////////////////////////////////////////////////
   
   const double g    = 1.32471795724474602596;
   const double a[2] = {1.0, 7.0};

   void generate_seeds() {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<> dist(0.0, 1.0);
      for(uint32_t i=0; i<R*R; ++i) {
         this->seeds[i] = rand();
      }

      for(uint32_t i=0; i<R*R; ++i)
      {
         this->samples[i] = Eigen::MatrixXf(NbSPP, NbDim);
#if defined(SAMPLES_1D_RANK_LATTICE)
         for(uint32_t d=0; d<NbDim; ++d)
         {
            const double shift = dist(gen);
            for(uint32_t k=0; k<NbSPP; ++k)
            {
               //const double xi = (k * a[d]) / double(NbSPP);
               //this->samples[i](k,d) = (float)fmod(xi  + shift, 1.0);
               this->samples[i](k,d) = _rank1.rank1_float(k, d, shift);
            }
         }

#elif defined(SAMPLES_RANDOM)
         for(uint32_t k=0; k<NbSPP; ++k)
            for(uint32_t d=0; d<NbDim; ++d)
            {
               this->samples[i](k,d) = dist(gen);
            }

#elif defined(SAMPLES_OWEN_ANTITHETIC)
         for(uint32_t k=0; k<NbSPP/2; ++k)
            for(uint32_t d=0; d<NbDim; ++d)
            {
               this->samples[i](k,d) = _sobol.sobol_owen(k, d, 32u, this->seeds[i]);
               this->samples[i](k + NbSPP/2, d) = 1.0f - this->samples[i](k, d);
            }

#elif defined(SAMPLES_OWEN)
         for(uint32_t d=0; d<NbDim; ++d)
         {
            uint32_t seed = gen();
            for(uint32_t k=0; k<NbSPP; ++k)
            {
               this->samples[i](k,d) = _sobol.sobol_owen(k, d, 32u, seed);
            }
         }

#elif defined(SAMPLES_OWEN_XOR)
         for(uint32_t d=0; d<NbDim; ++d)
         {
            uint32_t seed = gen();
            for(uint32_t k=0; k<NbSPP; ++k)
            {
               uint32_t y = _sobol.sobol_owen_binary(k, d, 32u, this->seeds[0]);
               y = y ^ seed;
               this->samples[i](k,d) = _sobol.binary_to_float(y);
            }
         }

#elif defined(SAMPLES_SOBOL_LAINE_KARRAS)
         for(uint32_t d=0; d<NbDim; ++d)
         {
            uint32_t seed = gen();
            for(uint32_t k=0; k<NbSPP; ++k)
            {
               uint32_t k2 = _sobol.laine_karras_scramble(k, this->seeds[i]);
               this->samples[i](k,d) = _sobol.sobol_laine_karras(k2, d, seed);
            }
         }
#endif
      }
   }

   Eigen::VectorXf eval(uint32_t p) {
      Eigen::VectorXf fint(NbIntegrands);
      for(uint32_t i=0; i<NbIntegrands; ++i)
      {
         fint(i) = eval_heaviside(this->samples[p], i);
      }
      return fint;
   }

   void eval_tile() {
      for(uint32_t j=0; j<this->R; ++j)
         for(uint32_t i=0; i<this->R; ++i)
         {
            const uint32_t index = i + j*this->R;
            this->fvalues[index] = this->eval(index);
         }
   }

   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                    Mutation part                           //
   //                                                            //
   ////////////////////////////////////////////////////////////////

   /* Evaluate the cost function around pixel p(i,j) when its value
    * is fp.
    */
   float eval_cost(const uint32_t p[2], const Eigen::VectorXf& fp)
   {
      float cost = 0.0f;
      const int32_t  W = 7;
      const uint32_t R = this->R;
      for(int32_t u=-W; u<=W; ++u)
         for(int32_t v=-W; v<=W; ++v)
         {
            if( u == 0 && v == 0) { continue; }

            const uint32_t i = (p[0] + u + R) % R;
            const uint32_t j = (p[1] + v + R) % R;
            const uint32_t index = i + j*R;

            const Eigen::VectorXf& fq = this->fvalues[index];
            const float w = expf( - (u*u + v*v) / 2.1f);
            cost += w * (fq-fp).norm();
         }
      return cost;
   }

   bool permut_pixels(const uint32_t pA[2], const uint32_t pB[2])
   {
      const Eigen::VectorXf& fA = this->fvalues[pA[0] + this->R*pA[1]];
      const Eigen::VectorXf& fB = this->fvalues[pB[0] + this->R*pB[1]];

      const float costsA_old = this->eval_cost(pA, fA);
      const float costsB_old = this->eval_cost(pB, fB);

      const float costsA_new = eval_cost(pA, fB);
      const float costsB_new = eval_cost(pB, fA);

      const float costNew = (costsA_new + costsB_new);
      const float costOld = (costsA_old + costsB_old);

      const bool accept_mut = costNew > costOld;
      if( accept_mut ) {
         this->swap_values(pA, pB);
      }

      return accept_mut;
   }

   void swap_values(const uint32_t pA[2], const uint32_t pB[2])
   {
      uint32_t iA = pA[0] + this->R*pA[1];
      uint32_t iB = pB[0] + this->R*pB[1];

      std::swap(this->fvalues[iA], this->fvalues[iB]);
      std::swap(this->seeds[iA], this->seeds[iB]);
      std::swap(this->samples[iA], this->samples[iB]);
   }

   void optimize(uint32_t nb_mut) {
      std::random_device rd;  //Will be used to obtain a seed for the random number engine
      std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
      std::uniform_int_distribution<uint32_t> dist(0, this->R-1);

      uint32_t accepted_mut = 0u;
      for(uint32_t k=0; k<nb_mut; ++k) {
         uint32_t pA[2] = { dist(gen), dist(gen) };
         uint32_t pB[2] = { dist(gen), dist(gen) };

         bool has_permuted = permut_pixels(pA, pB);
         accepted_mut += (uint32_t)has_permuted;
      }

      std::cout << accepted_mut << std::endl;
   }


   ////////////////////////////////////////////////////////////////
   //                                                            //
   //                      IO routines                           //
   //                                                            //
   ////////////////////////////////////////////////////////////////

   void save_tile_mts(const std::string filename) {
      
      std::ofstream file(filename.c_str());
      file << this->R << std::endl;
      file << this->NbSPP << std::endl;
      file << this->NbDim << std::endl;

      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            Eigen::MatrixXf& samples = this->samples[i + j*this->R];
            for(uint32_t k=0; k<this->NbSPP; ++k) {
               for(uint32_t d=0; d<this->NbDim; ++d) {
                  float sample = samples(k,d);
                  file << sample << std::endl;
               }
            }
         }
      }
   }
   void load_tile_mts(const std::string filename) {
      std::ifstream file(filename.c_str());
      unsigned int t;
      file >> t; this->R = t;
      file >> t; this->NbSPP = t;
      file >> t; this->NbDim = t;
      
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            this->samples[i + j*this->R] = Eigen::MatrixXf(this->NbSPP, this->NbDim);
            for(uint32_t k=0; k<this->NbSPP; ++k) {
               for(uint32_t d=0; d<this->NbDim; ++d) {
                  float sample;
                  file >> sample;
                  this->samples[i + j*this->R](k,d)= sample;
               }
            }
         }
      }
   }

   void save_tile(const std::string filename) {
      auto img = cimg_library::CImg<float>(this->R, this->R, 1, 3);
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            auto fv = this->fvalues[i + j*this->R];
            img(i, j, 0) = 255.0f * fv(0);
            img(i, j, 1) = 255.0f * fv(0);
            img(i, j, 2) = 255.0f * fv(0);
         }
      }
      img.save(filename.c_str(), 0);

      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            auto fv = this->fvalues[i + j*this->R];
            img(i, j, 0) = 255.0f * fv(2);
            img(i, j, 1) = 255.0f * fv(2);
            img(i, j, 2) = 255.0f * fv(2);
         }
      }
      img.save(filename.c_str(), 1);

      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            auto fv = this->fvalues[i + j*this->R];
            img(i, j, 0) = 255.0f * fv(NbIntegrands-1);
            img(i, j, 1) = 255.0f * fv(NbIntegrands-1);
            img(i, j, 2) = 255.0f * fv(NbIntegrands-1);
         }
      }
      img.save(filename.c_str(), 2);
   }

   void save_xy_tile(const std::string filename) {
      auto img = cimg_library::CImg<float>(this->R, this->R, 1, 3);
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            const auto index = i + j*this->R;
            const auto seed  = this->seeds[index];
            auto fv = eval_xy(this->samples[index]);
            img(i, j, 0) = 255.0f * fv;
            img(i, j, 1) = 255.0f * fv;
            img(i, j, 2) = 255.0f * fv;
         }
      }
      img.save(filename.c_str());
   }

   void save_cos_tile(const std::string filename) {
      auto img = cimg_library::CImg<float>(this->R, this->R, 1, 3);
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            const auto index = i + j*this->R;
            const auto seed  = this->seeds[index];
            auto fv = eval_cos(this->samples[index], 0);
            img(i, j, 0) = 255.0f * fv;
            img(i, j, 1) = 255.0f * fv;
            img(i, j, 2) = 255.0f * fv;
         }
      }
      img.save(filename.c_str());
   }

   void visu_integrand(const std::string filename, uint32_t k) {
      auto img = cimg_library::CImg<float>(this->R, this->R, 1, 3);
      for(uint32_t j=0; j<this->R; ++j) {
         for(uint32_t i=0; i<this->R; ++i) {
            Eigen::MatrixXf samples(1, 2);
            samples(0, 0) = float(i) / float(this->R);
            samples(0, 1) = float(j) / float(this->R);
            auto fv = eval_heaviside(samples, k);
            img(i, j, 0) = 255.0f * fv;
            img(i, j, 1) = 255.0f * fv;
            img(i, j, 2) = 255.0f * fv;
         }
      }
      img.save(filename.c_str());
   }

   std::vector<uint32_t> seeds;
   std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf>> samples;
   std::vector<Eigen::VectorXf> fvalues;
};


int main(int argc, char** argv) {

   /* Global parameters of the algorithm: see command line args */
   uint32_t R = 128u; // Resolution of the tile
   uint32_t K = 16u;
   
   /* Temporary data */
   Tile tile(K, R);

#ifdef OPTIMIZE_TILE
   for(uint32_t k=0; k<16u; ++k) {
      std::string s;
      s = "integrand-" + std::to_string(k) + ".png";
      tile.visu_integrand(s, k);
   }
   tile.save_tile("tile-before.png");
   tile.save_xy_tile("tile-xy-before.png");
   tile.save_cos_tile("tile-cos-before.png");
   tile.save_tile_mts("tile.samples");

   for(uint32_t k=0; k<1000; ++k) {
      tile.optimize(10000);
      // if(k % 100 == 0) {
         tile.save_tile("tile-after.png");
         tile.save_xy_tile("tile-xy-after.png");
         tile.save_cos_tile("tile-cos-after.png");
         tile.save_tile_mts("tile.samples");
      // }
   }
   tile.save_tile("tile-after.png");
   tile.save_xy_tile("tile-xy-after.png");
   tile.save_cos_tile("tile-cos-after.png");
   tile.save_tile_mts("tile.samples");
#else // OUTPUT_TILE
   tile.save_tile_mts("tile.samples");
#endif
   return EXIT_SUCCESS;
}
