// STL includes
#include <cstdlib>

// Sobol includes
#include <sobol/sobol.hpp>

// cimg includes
#define cimg_display 0
#include <cimg/cimg.h>

// Local includes
#include "optimizer.hpp"
#include "dyadicity.hpp"

/* `optimizer.cpp`
 *
 * This tool tries to optimize the different dimensions of the Sobol sequence
 * to ensure the best possible convergence assuming the constraint of Monte
 * Carlo rendering. That is, we need to optimize incrementally the dimensions.
 *
 * We start by asusming that the first dimension (the radical inversion) is
 * optimal and incrementally add other dimensions.
 */

sobol* sampler = nullptr;

bool test_dyadicity(uint32_t dim_i, uint32_t dim_j)
{
   // 2D Point list
   std::vector<point> _points;
   for(uint32_t i=0; i<1024; ++i) {
      _points.push_back({ sampler->sobol_float(i, dim_i),
                          sampler->sobol_float(i, dim_j) });
   }

   return is_dyadic(_points);
}

void print_dyadicity(const std::string& filename, bool* dyadicity, uint32_t nb_dims)
{
      // Create the empy matrix
   cimg_library::CImg<unsigned char> image(nb_dims, nb_dims, 1u, 3u, 255u);

   for(uint32_t dim_i=0; dim_i<nb_dims; ++dim_i)
      for(uint32_t dim_j=0; dim_j<nb_dims; ++dim_j)
      {
         bool test = dyadicity[dim_i*nb_dims + dim_j];
         if(test) {
            image(dim_i, dim_j, 0u) = 255u;
            image(dim_i, dim_j, 1u) = 0u;
            image(dim_i, dim_j, 2u) = 0u;
         } else {
            image(dim_i, dim_j, 0u) = 0u;
            image(dim_i, dim_j, 1u) = 0u;
            image(dim_i, dim_j, 2u) = 0u;
         }
      }
   image.save(filename.c_str());
}

void permut_sobol(uint32_t i, uint32_t j)
{
   std::swap(sampler->sobol_aj[i], sampler->sobol_aj[j]);
   std::swap(sampler->sobol_dj[i], sampler->sobol_dj[j]);
   std::swap(sampler->sobol_sj[i], sampler->sobol_sj[j]);
   uint32_t temp_mk[CHAR_BIT*sizeof(uint32_t)];
   std::memcpy(temp_mk,              sampler->sobol_mk[i], CHAR_BIT*sizeof(uint32_t)*sizeof(uint32_t));
   std::memcpy(sampler->sobol_mk[i], sampler->sobol_mk[j], CHAR_BIT*sizeof(uint32_t)*sizeof(uint32_t));
   std::memcpy(sampler->sobol_mk[j], temp_mk,              CHAR_BIT*sizeof(uint32_t)*sizeof(uint32_t));
}

void compute_dyadicity(bool *dyadicity, uint32_t nb_dims)
{
   for(uint32_t dim_i=0; dim_i<nb_dims; ++dim_i)
      #pragma omp parallel for
      for(uint32_t dim_j=0; dim_j<nb_dims; ++dim_j)
      {
         if(dim_i == dim_j) {
            dyadicity[dim_i*nb_dims + dim_j] = false;

         } else if(dim_i > dim_j) {
            dyadicity[dim_i*nb_dims + dim_j] = dyadicity[dim_j*nb_dims + dim_i];

         } else {
            dyadicity[dim_i*nb_dims + dim_j] = test_dyadicity(dim_i, dim_j);
         }
      }
}

#define EXPORT_DYADICITY_IMAGE

int main(int argc, char** argv)
{
   // Allocate sampler
   sampler = new sobol("../data/new-joe-kuo-6.21201");
   const uint32_t nb_dims = 1024;

   // Precompute dyadicity
   bool dyadicity[nb_dims*nb_dims];
   compute_dyadicity(dyadicity, nb_dims);
   print_dyadicity("before.png", dyadicity, nb_dims);
   sampler->save_mk("sobol_before.binary", sobol::FORMAT_BINARY);

   // For a given column, rearrange the matrix to stack the good
   // dimensions at the beginning.
   for(uint32_t cur_i=0u; cur_i<nb_dims-1; ++cur_i) {
      std::cout << cur_i << std::endl;
      uint32_t cur_k = cur_i+1;

      while(cur_i == cur_k || dyadicity[cur_i*nb_dims + cur_k]) {
         ++cur_k;
      }

      for(uint32_t dim_j=cur_k; dim_j<nb_dims; ++dim_j)
      {
         if(dim_j == cur_i) continue;

         if(dyadicity[cur_i*nb_dims + dim_j]) {
            permut_sobol(cur_k, dim_j);
            compute_dyadicity(dyadicity, nb_dims);
            ++cur_k;
         }
      }
      
      std::string filename = "./outputs/after" + std::to_string(cur_i) + ".png";
      print_dyadicity(filename.c_str(), dyadicity, nb_dims);
      sampler->save_mk("./outputs/sobol_optimized.binary", sobol::FORMAT_BINARY);
   }
   std::string filename = "after.png";
   print_dyadicity(filename.c_str(), dyadicity, nb_dims);
   sampler->save_mk("sobol_optimized.binary", sobol::FORMAT_BINARY);

#ifdef EXPORT_DYADICITY_IMAGE


#else // PRINT NB DYADIC PER DIM
   for(uint32_t dim_i=0; dim_i<nb_dims; ++dim_i)
   {
      uint32_t nb_dyadic = 0u;
      for(uint32_t dim_j=0; dim_j<nb_dims; ++dim_j)
      {
         if(dim_i == dim_j) continue;
         if(test_dyadicity(dim_i, dim_j)) {
            ++nb_dyadic;
         }
      }
      std::cout << "Dim " << dim_i << " => " << nb_dyadic << std::endl;
   }
#endif

   delete sampler;

   return EXIT_SUCCESS;
}
