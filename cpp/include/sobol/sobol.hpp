#pragma once

// STL includes
#include <iostream>
#include <fstream>
#include <climits>
#include <cstring>
#include <random>
#include <stdint.h>
#include <cassert>

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

struct sobol
{
    /* `sobol_mk` stores the direction numbers for all the dimensions. For
     * example, the direction numbers for dimension `d` are stored in the
     * memory space:
     *
     *      sobol_mk[B*d] ... sobol_mk[B*d + B-1]
     *
     * where B is the number of bits of the index.
     *
     * For 32 bits indexing the number of direction numbers per dimension
     * is 32.
     *
     * `sobol_aj' stores the primitive polynomials.
     * `sobol_sj` stores the dimension of the primitive polynomials `sobol_aj`.
     */
    static const uint32_t sobol_n = 1024;
    uint32_t sobol_mk[sobol_n][CHAR_BIT*sizeof(uint32_t)];
    uint32_t sobol_aj[sobol_n];
    uint32_t sobol_sj[sobol_n];
    uint32_t sobol_dj[sobol_n];

    /* Constructor */
    sobol() {} // default

    sobol(const char* filename)
    {
        load_mk(filename);
    }

    /* Generate the direction numbers for dimension `dim` using the provided
     * polynomial `aj` of degree `sj` and the s initial direction numbers stored
     * in the first `sj` elements of vector `mk`.
     *
     * We assume that mk is already allocated to the number of bits composing
     * the integer type.
     */
    static inline
    void generate_mk(uint32_t aj, uint32_t sj, uint32_t* mk)
    {
        /* For each direction number, we apply the recurrence formula:
         *    m_k = aj_1 2 m_{k-1} xor ... xor aj_{sj} 2^{sj} m_{k-sj}
         *                                 xor mk_{k-sj}
         */
        const uint8_t size = CHAR_BIT*sizeof(uint32_t);
        for(uint8_t k=sj; k<size; ++k)
        {
            mk[k] = 0;
            for(uint8_t i=1; i<sj; ++i)
            {
                // `akj` stores aj_k, note that the polynomial rep is reversed
                // `pw2` stores 2^k
                const uint32_t akj = (aj >> (sj-1-i)) & 1;
                const uint32_t pw2 = (1 << i);
                mk[k] ^=  akj * pw2 * mk[k-i];
            }
            mk[k] ^= (1 << sj) * mk[k-sj];
            mk[k] ^= mk[k-sj];
        }

        /* Precompute the shifted version of the direction numbers
         * that generate the bit reversal.
         */
        for(uint8_t k=0; k<size; ++k)
        {
            mk[k] = mk[k] << (CHAR_BIT*sizeof(uint32_t) - (k+1u));
        }
    }


    /* Load the direction number, primite polynomials and associated dimensions
     * from a file. Depending on the file, we can load either the Joe and Kuo
     * file or a binary one.
     */
    inline
    void load_mk(const char* filename)
    {
        /* Failsafe */
        if(filename == nullptr)
        {
            std::cerr << __FILENAME__ << ":" << __LINE__ << " "
                      << "no direction numbers file provided "
                      << "make sure to use \'load_mk\'" << std::endl;
            return;
        }

        std::ifstream file(filename, std::ifstream::binary);
        char temp[6]; temp[5] = 0;
        file.read(temp, 5u * sizeof(char));

        /* Check if the file is supported
         * We support two kind of file format: the one of Joe and Kuo that
         * starts with "d\ts\ta" or one own binary format that starts with
         * "SOBOL".
         */
        if( strcmp(temp, "SOBOL") == 0 )
        {
            std::cerr << __FILENAME__ << ":" << __LINE__ << " "
                      << "\"" << filename << "\" "
                      << "is a binary Sobol file" << std::endl;

            file.read((char*)sobol_mk, sobol_n*CHAR_BIT*sizeof(uint32_t) * sizeof(uint32_t));
            file.read((char*)sobol_aj, sobol_n * sizeof(uint32_t));
            file.read((char*)sobol_sj, sobol_n * sizeof(uint32_t));
            file.read((char*)sobol_dj, sobol_n * sizeof(uint32_t));
        }
        else if( temp[0] == 'd' )
        {
            std::cerr << __FILENAME__ << ":" << __LINE__ << " "
                      << "\"" << filename << "\" "
                      << "is a Joe and Kuo file" << std::endl;
            file.ignore(256, '\n');

            // Load all the mk, aj and sj
            // We do not store more than 1110 dimension
            uint32_t index = 0;

            // Add the first polynom x^0
            sobol_aj[index] = 0;
            sobol_sj[index] = 0;
            sobol_dj[index] = 1;
            for(uint32_t k=0; k<32; ++k)
            {
                sobol_mk[index][k] = 1u << (CHAR_BIT*sizeof(uint32_t) - (k+1u));
            }
            index++;

            while( file.good() && index < sobol_n )
            {
                // Parse the polynomial
                uint32_t dj, sj, aj;
                file >> dj >> sj >> aj;
                sobol_aj[index] = aj;
                sobol_sj[index] = sj;
                sobol_dj[index] = dj;

                // Parse the direction numbers
                for(uint8_t i=0; i<sj; ++i)
                {
                    file >> sobol_mk[index][i];
                }

                // Fill the remaining direction numbers
                generate_mk(aj, sj, sobol_mk[index]);

                index++;
            }
        }
        else
        {
            std::cerr << "Unknown file format \"" << temp << "\" "
                      << "for Sobol file \"" << filename << "\" "
                      << "[" << __FILENAME__ << ":" << __LINE__ << "]"
                      << std::endl;
        }
    }

    /* Save the direction number, primite polynomials and associated dimensions
     * from a file. We can write the file into two different format: the joe and
     * kuo file format (where the direction numbers are not completely written),
     * and our binary file format where all the direction numbers are specified.
     */
    enum sobol_format
    {
        FORMAT_JOE_KUO = 0u,
        FORMAT_BINARY  = 1u
    };

    inline
    void save_mk(const char* filename, const sobol_format format = FORMAT_JOE_KUO) const
    {
        std::ofstream file(filename, std::ifstream::trunc);

        // Output the direction numbers in the format of Joe and Kuo
        if(format == FORMAT_JOE_KUO)
        {
            // Print header
            file << "d\ts\ta\tm_i" << std::endl;

            // Print content
            for(uint32_t d=0; d<sobol_n; ++d)
            {
                if(sobol_dj[d] == 0) continue;

                //file << (d+2) << "\t"
                file << sobol_dj[d] << "\t"
                     << sobol_sj[d] << "\t"
                     << sobol_aj[d] << "\t";
                for(uint8_t k=0; k<sobol_sj[d]; ++k)
                {
                    file << sobol_mk[d][k] << " ";
                }
                file << std::endl;
            }
        }
        // Output the direction numbers in a binary format
        else
        {
            file << "SOBOL";
            file.write((char*)sobol_mk, sobol_n * CHAR_BIT*sizeof(uint32_t) * sizeof(uint32_t));
            file.write((char*)sobol_aj, sobol_n * sizeof(uint32_t));
            file.write((char*)sobol_sj, sobol_n * sizeof(uint32_t));
            file.write((char*)sobol_dj, sobol_n * sizeof(uint32_t));
        }
    }

    /* Generate a floating point value from 'x' */
    inline
    static
    float binary_to_float(uint32_t x)
    {
        double r = double(x) / (1ull << (CHAR_BIT*sizeof(uint32_t)));
        return float(r);
    }

    /* `scrambling_owen`
     *
     * Owen's scrambling of binary representation `si`.
     *
     * The permutation tree is evaluated up to `m` digits. That mean that
     * it permuts the set [0 .. 2^m-1] to itself.
     *
     * One can set the random number generator seed using `seed`.
     */
    inline
    uint32_t scrambling_owen(uint32_t si, uint8_t m=8, uint32_t seed=0) const
    {
        // Create a random number generator with a specific seed. This rng is
        // a tree of random number that need to be accessed in a coherent maner
        // the random number are explorer in a breadth first order.
        static thread_local std::mt19937_64 gen;
        std::uniform_int_distribution<uint32_t> dist(0,1);

        // Variables
        uint32_t result = si;
        uint32_t shift  = 0u;
        uint32_t base   = 0u;
        const uint8_t wsize = CHAR_BIT*sizeof(uint32_t)-1;

        // For every bit of the Sobol binary representation of a float, apply a
        // random perturbation. Sobol's binary representation is reversed
        // (meaning the right-most bits are the smallest 1/2^N fraction. So the
        // tree indexing start at the left-most part of Sobol's binary rep.
        for(uint8_t level=0; level<m; ++level)
        {
            // Fetch the random perturbation. First, we need to ofset the rng
            // to select the correct next random number.
            gen.seed((seed + base + shift));
            const uint32_t p = dist(gen);

            // Fetch the bit to permut and apply the random permutation to it
            // `rlevel` index the bits starting from the left-most one.
            assert(m-1 >= level);
            const uint8_t rlevel = wsize - level;
            uint32_t b = (si >> rlevel) & 1u;

            // Set the bit
            result ^= p << rlevel;

            // Update the level and the binary representation
            shift = (shift << 1u) | (b ^ p);
            base += 1u << level;
        }

        return result;
    }


    /* `sobol_binary`
     *
     * Generate the binary representation of the `i`-th point of the Sobol
     * sequence using the `mk` direction number.
     *
     * Optionnally, one can specify a xoring scramble using `s`.
     */
    inline
    uint32_t sobol_binary(uint32_t i, const uint32_t* mk, uint32_t s=0u) const
    {
        // `x` is the result binary representation of a float
        uint32_t x = s;

        // For the k-th bit of the index, if this bit is one, accumulate
        // the k-th direction number modulo 2 (using a xor).
        for(uint8_t k=0; i; i>>=1u, ++k)
        {
            if(i & 1u)
            {
                x ^= mk[k];
            }
        }
        return x;
    }
    inline
    uint32_t sobol_binary(uint32_t i, uint32_t d, uint32_t s=0u) const
    {
        return sobol_binary(i, sobol_mk[d]);
    }


    /* `sobol_float`
     *
     * Compute the `i`-th element of the Sobol sequence using the `mk`
     * direction numbers represented as floating point values in [0,1[.
     */
    inline
    float sobol_float(uint32_t i, const uint32_t* mk, uint32_t s=0u) const
    {
        // Evaluate Sobol in binary form
        const uint32_t x = sobol_binary(i, mk, s);

        // Convert binary form to floats
        const float v = binary_to_float(x);
        return v;
    }

    /* `sobol_float`
     *
     * Compute the `i`-th element of the Sobol sequence for the `d`-th
     * dimension reoresented as floating point values in [0,1[.
     *
     * This function uses the direction numbers stored in the `sobol_mk` array.
     * Warning: `sobol_mk` must be initialized!
     */
    inline
    float sobol_float(uint32_t i, uint32_t d, uint32_t s=0u) const
    {
        return sobol_float(i, sobol_mk[d], s);
    }

    /* `sobol_owen`
     *
     * Compute the `i`-th element of the Sobol sequence for the dimension
     * stored in the `mk` direction numbers.
     *
     * The Sobol binary form is scrambled using Owen's scrambling on the
     * first `m` bits of the binary form.
     */
    inline
    uint32_t sobol_owen_binary(uint32_t i, const uint32_t* mk, uint8_t m=32u, uint32_t seed=0u) const
    {
        // Evaluate Sobol in binary form
        const uint32_t x = sobol_binary(i, mk);

        // Scramble the binary form
        const uint32_t y = scrambling_owen(x, m, seed);
        return y;
    }
    inline
    uint32_t sobol_owen_binary(uint32_t i, const uint32_t d, uint8_t m=32u, uint32_t seed=0u) const
    {
        // Evaluate Sobol in binary form
        const uint32_t x = sobol_binary(i, sobol_mk[d]);

        // Scramble the binary form
        const uint32_t y = scrambling_owen(x, m, seed);
        return y;
    }


    /* `sobol_owen`
     *
     * Compute the `i`-th element of the Sobol sequence for the dimension
     * stored in the `mk` direction numbers.
     *
     * The Sobol binary form is scrambled using Owen's scrambling on the
     * first `m` bits of the binary form.
     */
    inline
    float sobol_owen(uint32_t i, const uint32_t* mk, uint8_t m=32u, uint32_t seed=0u) const
    {
        // Evaluate the binary form
        const uint32_t y = sobol_owen_binary(i, mk, m, seed);

        // Convert binary form to floats
        const float v = binary_to_float(y);
        return v;
    }

    inline
    float sobol_owen(uint32_t i, uint32_t d, uint8_t m=32u, uint32_t seed=0u) const
    {
        return sobol_owen(i, sobol_mk[d], m, seed);
    }

    /* `sobol_laine_karras`
     *
     *  Uses Laine and Karras' scrambling method [2011] to perturb the Sobol
     * sequence.
     */
    inline
    float sobol_laine_karras(uint32_t i, uint32_t d, uint32_t seed=0u) const
    {
        // Evaluate Sobol in binary form
        const uint32_t x = sobol_binary(i, sobol_mk[d]);

        // Apply Laine and Karras permutation with seed
        const uint32_t y = laine_karras_scramble(x, seed);
        
        // Convert binary form to floats
        const float v = binary_to_float(y);
        return v;
    }

    /* `laine_karras_permutation`
     * 
     * Implements the scrambling described by Laine and Karras [2011] and
     * further analyzed by Burley [2020]. This implementation is a copy of
     * Burley's implementation.
     */
    uint32_t reverse_bits(uint32_t x) const
    {
        x = ((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1);
        x = ((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2);
        x = ((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4);
        x = ((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8);
        x = ((x & 0xffff0000) >> 16) | ((x & 0x0000ffff) << 16);
        return x;
    }

    uint32_t laine_karras_scramble(uint32_t x, uint32_t seed) const
    {
        x = reverse_bits(x);
        x = laine_karras_permutation(x, seed);
        x = reverse_bits(x);
        return x;
    }

    inline
    uint32_t laine_karras_permutation(uint32_t x, uint32_t seed) const
    {
        x += seed;
        x ^= x*0x6c50b47cu;
        x ^= x*0xb82f1e52u;
        x ^= x*0xc7afe638u;
        x ^= x*0x8d22f6e6u;
        return x;
    }
};
