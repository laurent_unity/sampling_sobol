#pragma once

// STL include
#include <bitset>

// Eigen include
#include <Eigen/Core>


struct pmj02
{

    /* Do rejection sampling to generate a new sample (x,y) in a given sub-interval
     * defined by (OffsetX, OffsetY) and (WidthX, WidthY). A sample is accepted if
     * it doesn't overlap with another sample from Samples within a (0,2)-grid.
     */
    void generateSubInterval(const Eigen::MatrixXf& Samples,
                             unsigned int NbSamples,
                             unsigned int Subdivision,
                             std::mt19937 gen,
                             std::uniform_real_distribution<float> dist,
                             float  OffsetX,  float  OffsetY,
                             float  WidthX,   float  WidthY,
                             float& SampleX,  float& SampleY);

    /* Generate NbSamples using Pixar's `Progressive Multi-Jitter' method.
     * We generate random elements inside a subdivided grid and progressively
     * subdivide it while keeping track of non collisions in 1D strips.
     */
    Eigen::MatrixXf generateSamples(unsigned int NbSamples,
                                    unsigned int Dimensions,
                                    unsigned int Seed = 0)
    {
        assert(Dimensions == 2);
        Eigen::MatrixXf Result(NbSamples, Dimensions);

        // Random number generator
        std::mt19937 gen( Seed );
        std::uniform_real_distribution<float> dist(0.0f, 1.0f);

        // Start with a random first element
        Result(0,0) = dist(gen);
        Result(0,1) = dist(gen);

        // Loop over the remaining elements to fill the space
        unsigned int s=1;
        unsigned int Subdivision = 4;
        float Offset = 1.0f;
        while(s<NbSamples)
        {
            // For each sample already distributed, we generate the samples is 
            // the empty sub-cells of its current cell.
            const auto CurrentS      = s;
            const auto HalfOffset    = 0.5f * Offset;
            for(auto i=0; i<CurrentS; ++i)
            {
                const auto x = Result(i,0);
                const auto y = Result(i,1);

                const auto CellI = (unsigned int)( x / Offset );
                const auto CellJ = (unsigned int)( y / Offset );

                const auto SubI = (unsigned int)( (x-Offset*CellI) / HalfOffset );
                const auto SubJ = (unsigned int)( (y-Offset*CellJ) / HalfOffset );

                float ShiftI, ShiftJ, SampleX, SampleY;
                float OffsetX, OffsetY;
                ShiftI = float((SubI+1) % 2);
                ShiftJ = float((SubJ+1) % 2);
                OffsetX   = CellI*Offset + ShiftI*HalfOffset;
                OffsetY   = CellJ*Offset + ShiftJ*HalfOffset;
                generateSubInterval(Result, s, Subdivision, gen, dist, OffsetX, OffsetY, HalfOffset, HalfOffset, SampleX, SampleY);
                Result(s, 0) = SampleX;
                Result(s, 1) = SampleY;
                s++;
                if(s == NbSamples) { return Result; }

                ShiftI    = float((SubI+1) % 2);
                ShiftJ    = float(SubJ);
                OffsetX   = CellI*Offset + ShiftI*HalfOffset;
                OffsetY   = CellJ*Offset + ShiftJ*HalfOffset;
                generateSubInterval(Result, s, Subdivision, gen, dist, OffsetX, OffsetY, HalfOffset, HalfOffset, SampleX, SampleY);
                Result(s, 0) = SampleX;
                Result(s, 1) = SampleY;
                s++;
                if(s == NbSamples) { return Result; }

                ShiftI = float(SubI);
                ShiftJ = float((SubJ+1) % 2);
                OffsetX   = CellI*Offset + ShiftI*HalfOffset;
                OffsetY   = CellJ*Offset + ShiftJ*HalfOffset;
                generateSubInterval(Result, s, Subdivision, gen, dist, OffsetX, OffsetY, HalfOffset, HalfOffset, SampleX, SampleY);
                Result(s, 0) = SampleX;
                Result(s, 1) = SampleY;
                s++;
                if(s == NbSamples) { return Result; }
            }

            Offset *= 0.5f;
            Subdivision *= 4;
        }

        return Result;
    }

    /* `generateSubInterval` implementation
     */
    void generateSubInterval(const Eigen::MatrixXf& Samples,
                             unsigned int NbSamples,
                             unsigned int Subdivision,
                             std::mt19937 gen,
                             std::uniform_real_distribution<float> dist,
                             float  OffsetX,  float  OffsetY,
                             float  WidthX,   float  WidthY,
                             float& SampleX,  float& SampleY)
    {
        bool Criteria = false;
        while(!Criteria)
        {
            // Generate a new sample
            SampleX = OffsetX + WidthX * dist(gen);
            SampleY = OffsetY + WidthY * dist(gen);

            // Check all projections
            Criteria = true;
            for(unsigned int s=0; s<NbSamples; ++s)
            {
                const float x = Samples(s, 0);
                const float y = Samples(s, 1);

                for(unsigned int sx=1; sx<= Subdivision; sx*=2)
                {
                    unsigned int sy = Subdivision / sx;

                    // Width of the xy intervals
                    const float SubWidthX = 1.0 / float(sx);
                    const float SubWidthY = 1.0 / float(sy);

                    // Index in the sub-interval
                    const int IndexX = SampleX / SubWidthX;
                    const int IndexY = SampleY / SubWidthY;

                    const int SampleIndexX = x / SubWidthX;
                    const int SampleIndexY = y / SubWidthY;

                    if(IndexX == SampleIndexX && IndexY == SampleIndexY)
                    {
                        Criteria = false;
                    }
                }
            }
        }
    }
};
