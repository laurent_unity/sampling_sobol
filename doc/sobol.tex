\documentclass[format=acmtog,authorversion=true]{acmart}

%%% Title of your article or abstract.
\title[Sobol Sequence]{Looking at the Sobol Sequence}

\author{Laurent Belcour}
\affiliation{Unity Technologies}
\email{laurent@unity3d.com}

% \keywords{Iridescence, SV-BRDF model, spectral aliasing, thin-film interference}

%%% The next five lines define the rights management block on the first page.
%%% Replace them with the LaTeX commands provided when the form has been completed.
% \setcopyright{acmlicensed}
% \acmJournal{TOG}
% \acmYear{2017}\acmVolume{36}\acmNumber{4}\acmArticle{65}\acmMonth{7} \acmDOI{http://dx.doi.org/10.1145/3072959.3073620}

\citestyle{acmauthoryear}
\setcitestyle{square}

%%% Start of the document.

\begin{document}

%%% This is the ``teaser'' command, which puts an figure, centered, below
%%% the title and author information, and above the body of the content.
% \include{fig-chair}

% \begin{abstract}
% \end{abstract}

% \begin{CCSXML}
% <ccs2012>
% <concept>
% <concept_id>10010147.10010371.10010372.10010376</concept_id>
% <concept_desc>Computing methodologies~Reflectance modeling</concept_desc>
% <concept_significance>500</concept_significance>
% </concept>
% </ccs2012>
% \end{CCSXML}

% \ccsdesc[500]{Computing methodologies~Reflectance modeling}

\maketitle

\section{The Sobol Sequence}
\subsection{Generating the Sequence}
Sobol sequence can be generated using either the generating matrices $C_k$ or the fundemental polynomials $p_k$ (see Dick and Phillichshammer). Here, we describe how to produce a point in $[0,1]^2$ from its index $n$ using the former method using the base $b = 2$. Each point of the Sobol sequence is a rational vector of the form:
\begin{align}
    \mathbf{p}_n = \sum_{k_0}^{K} \left[x_n^k, y_n^k \right] \dfrac{1}{2^{k+1}}, \quad \mbox{where}~x_n^k, y_n^k \in \{0,1\}.
\end{align}
We can see this form as placing points at the crossings of a quadtree. The question remains to generate the quadtree index vector $\mathbf{x}_n = \left[ x_n^k \right]$, ${k\in[0,K]}$ (refered as quadtree vector) from the sequence index $n$. Starting from the base $2$ expanson of $n$:
\begin{align}
    n = \sum_{k=0}^{K} n_k \, 2^k, \quad \mbox{with}~n_k \in \{0,1\},
\end{align}
we define the index vector $\mathbf{n} = [n_0, \cdots, n_K]$. The quadtree vector for each componnent is computed as:
\begin{align}
    \mathbf{x}_n = \mathbf{n} \times C_x
\end{align}
We do similarly with componnent $\mathbf{y}_n$ of the point $\mathbf{p}_n$. Note that the addition and multiplication used to compute the matrix product are done in the ring $\mathbb{Z}_2$ (that is $1+1 =_2 0$).\\

Since the sequence is a mapping of the binary decomposition of the index to the index in a quadtree subdivision, some bits of $n$ will correspond to levels in the quadtree.


\subsection{Quadtree Approach}
It is possible to generate points not with respect to their index, but with respect to their coordinate and have a hierarchical generation approach. That is, it is possible to generate every points falling in a given quadtree leaf for a given maximum number of samples (see Fig.~\ref{fig:quadtree_generation}).\\
\begin{figure}[h]
    \includegraphics[height=0.47\linewidth]{../figures/pdf/sobol_64pts.pdf}
    \includegraphics[height=0.47\linewidth]{../figures/pdf/sobol_populate_quadtree.pdf}
    \caption{We can generate all points in a given quadtree cell using the matrix approach.
    \label{fig:quadtree_generation}}
\end{figure}

For $N = 2^{k d}$, where $k > 0$ and $d$ is the dimension, the Sobol sequence uniformly distributes points on each component with a spacing of $\delta = 2^{-k d}$. That is, we can index the sequence by one of its coordinates. We use this property and start from the x-component. Given $p_x$, how can we evaluate $p_y$ and so on? This is given by the conversion matrix $T_{xy}$:
\begin{align}
    \mathbf{y}_n = \mathbf{x}_n \times T_{xy},
\end{align}
with $T_{xy} = C_x^{-1} C_y$.

\paragraph{Example in 2D}
Now let say that we can to generate all the points falling in a quadtree leaf. We will index this leaf using its binary bottom-left coordinates in x and y: $\left[ 1 1 0, 0 0 1 \right] \rightarrow \left[ 2^{-1} + 2^{-2},  2^{-3} \right]$. Every point in this cell will have a binary coordinate of $x = \left[ 0 0 1 \, x_3 \cdots x_n \right]$, and $y = \left[ 1 1 0 \, y_3 \cdots y_n \right]$. We can write the constraint of falling into the cell $\left[ 1 1 0, 0 0 1 \right]$ as:
\begin{align}
    \left[ 0 0 1 \, y_3 \cdots y_n \right] = \left[ 1 1 0 \, x_3 \cdots x_n \right] \times T_{xy}.
\end{align}
The first three componnents of this vector equality provides a set of equations with only the $x_i$ unkown. This set is underconstrained if $n > 5$ and overconstrainted if $n < 5$. We develop here the case where $n = 5$. We will name $\bar{T}_{xy}$ the square sub-matrix of $T_{xy}$ corresponding to $[x_3 \, x_4 \, x_5]$. With this notation, we can write:
\begin{align}
    [001] = \bar{b}_{xy} + [x_3 \, x_4 \, x_5] \times \bar{T}_{xy},
\end{align}
where $\bar{b}_{xy}$ is the product of the first elements of $\mathbf{x}_n$ and $T_{xy}$. We can write down $[x_3 \, x_4 \, x_5]$ as:
\begin{align}
    [x_3 \, x_4 \, x_5] = \left( [001]-\bar{B}_{xy} \right) \times \bar{T}_{xy}^{-1}.
\end{align}
In Fig.~\ref{fig:quadtree_generation}, we populate the different sub-cells of the blue cell by setting the third bit of $\mathbf{x}_n$ and $\mathbf{y}_n$ to either $0$ or $1$. Note that to exactly constrain this set of equations, we need as many unknowns as there are bit already set.

\paragraph{Extension to nD}
To extend this method to more dimensions, we need to express the transfer matrix from the x-dimension to every dimension. Then, we use the same methodology and we end up with:
\begin{align}
    [001]_y = \bar{b}_{xy} + [x_3 \, \cdots \, x_N] \times \bar{T}_{xy} \\
    [011]_z = \bar{b}_{xz} + [x_3 \, \cdots \, x_N] \times \bar{T}_{xz},
\end{align}
for every dimension. We can regroup those equation in a single one using the concatenation of matrices:
\begin{align}
    [001 \, 011] = [\bar{b}_{xy} \, \bar{b}_{xz}] + [x_3 \, \cdots \, x_N] \times \left[\bar{T}_{xy} \, \bar{T}_{xz}\right].
\end{align}
This equation can be formulated if $N = 8$ in this example.

\subsection{Pattern of the Sequence}
\begin{figure}[h]
    \includegraphics[height=0.47\linewidth]{../figures/pdf/sobol_structure_16pts.pdf}
    \includegraphics[height=0.47\linewidth]{../figures/pdf/sobol_structure_64pts.pdf}
    \caption{The $16$ or $64$ points of the 2D Sobol sequence have a repetitive pattern with structural alignments. The first four points are colored in red, the next twelve points in blue and the remaining in green.
    \label{fig:sobol_structures}}
\end{figure}

The sequence posesses a structure and repetition of patterns. The distribution of points some of the quadtree cells are the same at some level. See Fig.~\ref{fig:sobol_structures} for an example at $N = 16$ and $N = 64$. We can see that the points in cells aligned with the diagonal or anti-diagonal (up to a wrapping of the domain) follow the same positions.




\subsection{Questions}

\textbf{What points fall in a given quadtree cell?} This poses the question of whether the sequence can be generated recursively compare to iteratively.



\subsection{Metrics on Point Distributions}
Starting from the distribution associated to a sequence of points $\mathbf{p}_i$, $i \in [1 .. N]$:
\begin{align}
    s(\mathbf{x}) = \sum \delta(\mathbf{x} - \mathbf{p}_i),
\end{align}
we can define several functions that helps to assess its quality. One possibility often used in the \textit{blue-noise} sampling paper is to analyze the point distribution in the Fourier domain and to extract its radially averaged power spectrum:
\begin{align}
    P(\nu) = \dfrac{1}{N} \left| \hat{s}(\nu) \right|^2,
\end{align}
where $ \hat{s}(\nu)$ is the Fourier transform of $s(r)$ with:
\begin{align}
    \hat{s}(\nu) = \int_{0}^{2\pi} \hat{s}\left(\nu \, \left[\cos(\phi), \sin(\phi)\right]\right) \mbox{d}\phi,
\end{align}

\begin{itemize}
    \item Talk about alternative metrics: discrepancy.
    \item Talk about alternative metrics: fourier spectrum.
    \item Talk about alternative metrics: pair correlation function.
    \item Talk about hyper-uniformity
\end{itemize}


% \bibliographystyle{acmsiggraph}
% \bibliography{paper}

\end{document}
