import math
import numpy as np
import scipy.special
import matplotlib.pyplot as plt

b = 2
m = 6
N = b**m
print('Generating Sobol for n=' + str(N))

def index2vector(n, b, m):
    bk = 1
    nk = np.empty([m], int)
    for k in range(0, m):
        nk[k] = (n/bk) % b
        bk *= b
    return nk

def vector2real(n, b, m):
    ob = 1.0/b
    bk = ob
    xn = 0.0
    for k in range(0, m):
        xn += n[k]*bk
        bk *= ob
    return xn

C1 = np.matrix(np.zeros([m,m], int))
C2 = np.matrix(np.zeros([m,m], int))
for k in range(0, m):
    C1[k,k] = 1
    #C2[k,m-(k+1)] = 1
    for l in range(k, m):
        C2[k,l] = scipy.special.binom(l, k) % b

C1 = C1.transpose()
C2 = C2.transpose()
print('C1 = \n' + str(C1))
print('C2 = \n' + str(C2))

iC1 = np.linalg.inv(C1)
iC2 = np.linalg.inv(C2)
print('inverse(C1) = \n' + str(iC1))
print('inverse(C2) = \n' + str(iC2))

T12 = iC1*C2
T21 = iC2*C1
print('transfert matrix iC1 x C2 = \n' + str(T12))
print('transfert matrix iC2 x C1 = \n' + str(T21))

fig, ax = plt.subplots()

B12 = T12[0:2, 0:2]
P12 = np.linalg.inv(T12[2:4, 0:2])
print(B12)
print(P12)

xy = np.empty([N,2], float)
for n in range(0, N):
    nk = index2vector(n, b, m)

    # if nk[0] > 0 or nk[m-1] > 0:
    #     continue

    # print(nk)
    nk1 = np.mod((nk*C1).A[0], b)
    nk2 = np.mod((nk*C2).A[0], b)
    # print(nk1)
    # print(nk2)
    # print(np.mod(nk1*T12, b))
    #if nk1[0] == 1 and nk1[1] == 1 and nk2[0] == 0 and nk2[1] == 1:
    # print()
    # print(nk)
    test_vec = (nk2[0:2] - nk1[0:2]*B12) * P12
    test_nk2 = np.mod(test_vec.A[0], b)
    # print(str(test_nk2))

    y1 = vector2real(nk1, b, m)
    y2 = vector2real(nk2, b, m)
    #print([y1, y2])
    xy[n, 0] = y1
    xy[n, 1] = y2

    nx = np.mod((nk1*iC1).A[0], b)
    ny = np.mod((nk2*iC2).A[0], b)
    # print(nx)
    # print(ny)

N1 = b**2
plt.plot(xy[0:N1,0], xy[0:N1,1], 'ro')
N2 = b**4
plt.plot(xy[N1:N2,0], xy[N1:N2,1], 'bo')
N3 = b**6
plt.plot(xy[N2:N3,0], xy[N2:N3,1], 'go')
plt.plot(xy[N3:N,0], xy[N3:N,1], 'ko')
# xy = np.empty([N,2], float)
# for n in range(0, N):
#     nk = index2vector(n, b, m)

#     if nk[0] < 1 or nk[m-1] < 1:
#         continue

#     nk1 = (nk * C1).A[0]
#     nk2 = (nk * C2).A[0]

#     y1 = vector2real(nk1, b, m)
#     y2 = vector2real(nk2, b, m)
#     #print([y1, y2])
#     xy[n, 0] = y1
#     xy[n, 1] = y2

# plt.plot(xy[:,0], xy[:,1], 'b+')

m_min = 1
N = b**(min(m, m_min))
ax.set_xticks(np.linspace(0, 1, N+1))
ax.set_yticks(np.linspace(0, 1, N+1))
plt.grid()
#plt.axis([0.25,0.75,0.25,0.75], 'equal')
plt.axis('square')
plt.axis([0.0,1.0,0.0,1.0])


# For n = 16 points
ax.add_patch(plt.Rectangle((0.0, 0.0), 0.5, 0.5, facecolor="#d3dce5"))
ax.add_patch(plt.Rectangle((0.5, 0.0), 0.5, 0.5, facecolor="#ffebea"))
ax.add_patch(plt.Rectangle((0.0, 0.5), 0.5, 0.5, facecolor="#ffebea"))
ax.add_patch(plt.Rectangle((0.5, 0.5), 0.5, 0.5, facecolor="#d3dce5"))


# For n = 64 points
# ax.add_patch(plt.Rectangle((0.00, 0.00), 0.25, 0.25, facecolor="#d3dce5"))
# ax.add_patch(plt.Rectangle((0.25, 0.75), 0.25, 0.25, facecolor="#d3dce5"))
# ax.add_patch(plt.Rectangle((0.50, 0.50), 0.25, 0.25, facecolor="#d3dce5"))
# ax.add_patch(plt.Rectangle((0.75, 0.25), 0.25, 0.25, facecolor="#d3dce5"))

# ax.add_patch(plt.Rectangle((0.00, 0.25), 0.25, 0.25, facecolor="#ffebea"))
# ax.add_patch(plt.Rectangle((0.25, 0.50), 0.25, 0.25, facecolor="#ffebea"))
# ax.add_patch(plt.Rectangle((0.50, 0.75), 0.25, 0.25, facecolor="#ffebea"))
# ax.add_patch(plt.Rectangle((0.75, 0.00), 0.25, 0.25, facecolor="#ffebea"))

# ax.add_patch(plt.Rectangle((0.00, 0.50), 0.25, 0.25, facecolor="#fff0bc"))
# ax.add_patch(plt.Rectangle((0.25, 0.25), 0.25, 0.25, facecolor="#fff0bc"))
# ax.add_patch(plt.Rectangle((0.50, 0.00), 0.25, 0.25, facecolor="#fff0bc"))
# ax.add_patch(plt.Rectangle((0.75, 0.75), 0.25, 0.25, facecolor="#fff0bc"))

# ax.add_patch(plt.Rectangle((0.00, 0.75), 0.25, 0.25, facecolor="#d7e5d2"))
# ax.add_patch(plt.Rectangle((0.25, 0.00), 0.25, 0.25, facecolor="#d7e5d2"))
# ax.add_patch(plt.Rectangle((0.50, 0.25), 0.25, 0.25, facecolor="#d7e5d2"))
# ax.add_patch(plt.Rectangle((0.75, 0.50), 0.25, 0.25, facecolor="#d7e5d2"))

plt.title('The first ' + str(b**m) + ' points of the Sobol sequence')
# plt.savefig('../figures/pdf/sobol_structure_16pts.pdf', bbox_inches='tight')
# plt.show()




## Evaluating difference of positions


fig, ax = plt.subplots()
plt.plot(xy[:,0], xy[:,1], 'ro')
m_min = 2
N = b**(min(m, m_min))
ax.set_xticks(np.linspace(0, 1, N+1))
ax.set_yticks(np.linspace(0, 1, N+1))
plt.grid()
plt.axis('square')
plt.axis([0.0,1.0,0.0,1.0])
plt.title('The first ' + str(b**m) + ' points of the Sobol sequence')
plt.savefig('../figures/pdf/sobol_64pts.pdf', bbox_inches='tight')



b = 2
m = 8
N = b**m
print('Generating Sobol for n=' + str(N))

C1 = np.matrix(np.zeros([m,m], int))
C2 = np.matrix(np.zeros([m,m], int))
for k in range(0, m):
    C1[k,k] = 1
    #C2[k,m-(k+1)] = 1
    for l in range(k, m):
        C2[k,l] = scipy.special.binom(l, k) % b

C1 = C1.transpose()
C2 = C2.transpose()

iC1 = np.linalg.inv(C1)
iC2 = np.linalg.inv(C2)

T12 = iC1*C2
T21 = iC2*C1

B12 = T12[0:3, 0:3]
P12 = np.linalg.inv(T12[3:6, 0:3])

xy = np.zeros([4,2], float)

nk1_b = np.zeros([m], int)
nk2_b = np.zeros([m], int)
nk1_b[0] = 1
nk2_b[1] = 1
print(nk1_b)
print(nk2_b)

for xi in [0,1]:
    for yi in [0,1]:
        nk1 = np.copy(nk1_b)
        nk2 = np.copy(nk2_b)
        nk1[2] = xi
        nk2[2] = yi

        test_vec = (nk2[0:3] - nk1[0:3]*B12)
        test_nk1 = nk1
        test_nk2 = nk2
        # print(test_nk1)
        # print(test_nk2)

        #test_nk1[3:6] = np.mod(test_vec.A[0], b)
        test_nk1[3:6] = np.mod((test_vec * P12), b)
        test_nk2 = np.mod(test_nk1 * T12, b).A[0]
        # print(test_nk1)
        # print(test_nk2)


        test_n1 = np.mod(test_nk1*iC1, b)
        test_n2 = np.mod(test_nk2*iC2, b)
        # print(test_n1)
        # print(test_n2)

        x = vector2real(test_nk1, b, m)
        y = vector2real(test_nk2, b, m)
        #print([y1, y2])

        index = 2*xi+yi
        xy[index, 0] = x
        xy[index, 1] = y
# print(xy)

fig, ax = plt.subplots()
plt.plot(xy[:,0], xy[:,1], 'ro')

m_min = 2
N = b**(min(m, m_min))

x_b = vector2real(nk1_b, b, m)
y_b = vector2real(nk2_b, b, m)
print([x_b, y_b])
ax.add_patch(plt.Rectangle((x_b, y_b), 0.5**m_min, 0.5**m_min, facecolor="#d3dce5"))

ax.set_xticks(np.linspace(0, 1, N+1))
ax.set_yticks(np.linspace(0, 1, N+1))
plt.grid()
plt.axis('square')
plt.axis([0.0,1.0,0.0,1.0])

plt.title('Populating one leaf of the quadtree')
plt.savefig('../figures/pdf/sobol_populate_quadtree.pdf', bbox_inches='tight')

plt.show()